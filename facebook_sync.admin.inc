<?php

/**
 * @file
 * Contain the admin settings form.
 */

/**
 * Settings form
 */
function facebook_sync_settings_form($form, $form_state, $reset = FALSE) {

  return system_settings_form($form);
}
